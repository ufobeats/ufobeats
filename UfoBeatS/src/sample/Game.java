package sample;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Game extends Stage {
    // constantes pour la taille


    private double non;                                 // !!!!!!!!!!!! c'est quoi non ?
    private double nonNon;                              // !!!!!!!!!!!! c'est quoi nonnon ?

    private final static int tailleJoueur = 10;

    private final int largeur 	= WindowArea.getLargeurEcran() ;
    private final int hauteur	= WindowArea.getLongueurEcran() ;

    private final static int ecartBord = 50;
    private static int x = 300-tailleJoueur/2;          // position joueur
    private static int y = 300-ecartBord;
    private boolean finJeu = false;                     // !!!!!!!!!!!! prévoir la fin du jeu

    private static Circle 		joueur		= new Circle(0,0,tailleJoueur);       // définition du joueur
    private static Group 		scene 	= new Group();                              // scene, racine du Scene Graph

    private static HashMap<Integer, Projectile> listProjectileUsed = new HashMap<Integer, Projectile>();
    private static HashMap<Integer, Projectile> listProjectileUnUsed = new HashMap<Integer, Projectile>();
    private static HashMap<Integer, Circle> listCircle = new HashMap<Integer, Circle>();

    private static ArrayList<Integer> frame = new ArrayList<Integer>();


    private static HashMap<Integer, Laser> listLaserUsed = new HashMap<Integer, Laser>();
    private static HashMap<Integer, Laser> listLaserUnUsed = new HashMap<Integer, Laser>();




    public Game() {
        this.setTitle("Ufobeats");
        this.setResizable(false);
        Scene laScene = new Scene(creerContenu());

        this.setWidth(largeur);
        this.setHeight(hauteur);
        laScene.setOnKeyPressed(e-> testClavier(e));        // Commande pour gerer le joueur


        this.setScene(laScene);

    }



    public static Parent creerContenu() {
        //joueur
        joueur.setFill(Color.BLUE);
        scene.getChildren().add(joueur);
        joueur.setLayoutY(y);
        joueur.setLayoutX(x);

        for(int i=0; i<1000; i++){
            Projectile projectile = new Projectile(0, 0, 0, 0, 0, 0);
            addProjectileUnUsed(projectile);
            scene.getChildren().add(listProjectileUnUsed.get(projectile.getId()).getSprite());
        }

        for(int i=0; i<20; i++){
            Laser laser = new Laser(20, 20, 0, 20, 500, 1000, false);
            addLaserUnUsed(laser);
            laser.getSprite().setFill(Color.TRANSPARENT);

            scene.getChildren().add(listLaserUnUsed.get(laser.getId()).getSprite());
        }

        return scene;
    }


    public static void updateSprite(int playerX , int playerY, int playerWidth){
        /* Fonction qui permet d'uptdate tous les sprites */
        if(!listProjectileUsed.isEmpty()) {
            for(int i= 0; i<frame.size(); i++){
                listProjectileUsed.remove(frame.get(i));
            }
            for(Map.Entry<Integer, Projectile> entry : listProjectileUsed.entrySet()){
                listProjectileUsed.get(entry.getKey()).moveSprite();
            }
        }
    }


    public static void setPosition(Projectile projectile){
        Circle sprite = listProjectileUsed.get(projectile.getId()).getSprite();
        sprite.setLayoutX(listProjectileUsed.get(projectile.getId()).getCoordX());
        sprite.setLayoutY(listProjectileUsed.get(projectile.getId()).getCoordY());

    }

    public static void addProjectileUnUsed(Projectile projectile){
        listProjectileUnUsed.put(projectile.getId(), projectile);

    }

    public static void addLaserUnUsed(Laser laser){
        listLaserUnUsed.put(laser.getId(), laser);
    }

    public static void addProjectile(Projectile projectile){
        int keyProj = listProjectileUnUsed.entrySet().iterator().next().getKey();
        Projectile projUse = listProjectileUnUsed.get(keyProj);
        listProjectileUnUsed.remove(keyProj);

        //set coord
        projUse.setCoordX(projectile.getCoordX());
        projUse.setCoordY(projectile.getCoordY());

        //set velocite
        projUse.setVelocite(projectile.getVelocite());

        //set vector
        projUse.setVectorX(projectile.getVectorX());
        projUse.setVectorY(projectile.getVectorY());

        listProjectileUsed.put(projUse.getId(), projUse);
    }

    public static void addlaser(Laser laser){
        int keyLaser = listLaserUnUsed.entrySet().iterator().next().getKey();
        Laser laserUse = listLaserUnUsed.get(keyLaser);

        listLaserUnUsed.remove(keyLaser);

        //set coord
        laserUse.setCoordX(laser.getCoordX());
        laserUse.setCoordY(laser.getCoordY());

        laserUse.getSprite().setLayoutX(laser.getCoordX());
        laserUse.getSprite().setLayoutY(laser.getCoordY());


        //set width
        if(laser.getPostion() == true){
            laserUse.getSprite().setWidth(laser.getWidth());
            laserUse.getSprite().setHeight(Integer.MAX_VALUE);
        }
        else{
            laserUse.getSprite().setWidth(Integer.MAX_VALUE);
            laserUse.getSprite().setHeight(laser.getWidth());
        }

        laserUse.showSprite();
        listLaserUsed.put(laserUse.getId(), laserUse);
    }

    public static HashMap<Integer, Projectile> getListProjectileUsed() {
        return listProjectileUsed;
    }


    public static void setListProjectileUsed(int id) {
        frame.add(id);

    }

    public static HashMap<Integer, Projectile> getListProjectileUnUsed() {
        return listProjectileUnUsed;
    }


    public static void setListProjectileUnUsed(HashMap<Integer, Projectile> list) {
        listProjectileUnUsed = list;
    }

    public static void getJoueur() {
        joueur.getLayoutX();
    }

    public HashMap<Integer, Circle> getCircle(){
        return listCircle;
    }


    public static void ajoutPreLaser(Laser laser) {
        laser.getSprite().setFill(Color.PINK);
    }
    public static void ajoutLaser(Rectangle sprite) {

        sprite.setFill(Color.RED);
    }
    public static void enleverLaser(Laser laser) {

        laser.getSprite().setFill(Color.TRANSPARENT);
    }








    private void testClavier(KeyEvent e) {
        if(e.getCode()==KeyCode.DOWN && joueur.getLayoutY()!=nonNon) {

            joueur.setLayoutY(joueur.getLayoutY()+5);

        }
        if(e.getCode()==KeyCode.UP && joueur.getLayoutY()!=non+ecartBord+tailleJoueur/4){
            joueur.setLayoutY(joueur.getLayoutY()-5);
        }
        if(e.getCode()==KeyCode.RIGHT && joueur.getLayoutX()!=nonNon-tailleJoueur/4) {
            joueur.setLayoutX(joueur.getLayoutX()+5);
        }
        if(e.getCode()==KeyCode.LEFT && joueur.getLayoutX()!=non+tailleJoueur/2) {
            joueur.setLayoutX(joueur.getLayoutX()-5);
        }
        System.out.println("X : " + joueur.getLayoutX());
        System.out.println("Y : " + joueur.getLayoutY());


        if(e.getCode()==KeyCode.A) {
            Laser laser = new Laser(100, 100, 10, 50, 500, 1000, true);
            Game.addlaser(laser);
        }


        if(e.getCode()==KeyCode.B) {
            finJeu=true;

        }
    }





}
