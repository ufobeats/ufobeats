package sample;

import java.util.ArrayList;

public abstract class Movable extends Entity{

    private double velocite;


    public Movable(int coordX, int coordY, int vectorX, int vectorY, double velocite) {
        super(coordX, coordY, vectorX, vectorY);
        this.velocite = velocite;
    }

    public double getVelocite() {
        return velocite;
    }

    public void setVelocite(double velocite) {
        this.velocite = velocite;
    }

    public abstract void moveSprite();
}
