package sample;


import javafx.scene.Node;
import javafx.scene.shape.Rectangle;

import java.util.Timer;
import java.util.TimerTask;

public class Laser extends Entity implements Obstacle {
    private boolean etat = false; // true -> degats actifs
    private int width;
    private int timeWarning;
    private int timeGlobal;
    private int id;
    private boolean position;	// vertical == false | horizontal == true

    private static int nb = 0;

    private Rectangle sprite;


    Timer timer = new Timer();

    public Laser(int coordX, int coordY, int widthx, int width, int timeWarning, int timeGlobal,boolean position) {
        super(coordX, coordY, widthx, 0);
        this.width = width;
        this.timeWarning = timeWarning;
        this.timeGlobal = timeGlobal;

        this.position = position;
        this.sprite = new Rectangle(coordX, coordY, width, Integer.MAX_VALUE);
        this.id = nb;
        nb++;
    }


    public float getX() {
        float coordx = this.getCoordX();
        return coordx;

    }
    public float getY() {
        float coordy = this.getCoordY();
        return coordy;
    }


    public int getWidth() {
        return width;
    }


    public void setWidth(int width) {
        this.width = width;
        sprite.setWidth(width);
    }



    public void setPosition(boolean position){
        this.position = position;
    }

    public boolean getPostion(){
        return position;
    }

    /*showSprite ici fait apparaitre le laser dans le jeu puis apr�s un certain temps peut
      entrer en colision avec le joueur avant de disparaitre
     */
    public void showSprite() {

        if(position==true) {
            Game.ajoutPreLaser(this);
            timer.schedule(new TimerTask() {
                public void run() {
                    etat = true;
                    Game.ajoutLaser(sprite);
                    System.out.println(position);
                    Game.getJoueur(); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                }
            }, timeWarning);

            timer.schedule(new TimerTask() {
                public void run() {
                    delete();
                    etat = false;
                    System.out.println(etat);
                }
            }, timeWarning+timeGlobal);


        }else {
            Game.ajoutPreLaser(this);
            timer.schedule(new TimerTask() {
                public void run() {
                    etat = true;
                    Game.ajoutLaser(sprite);
                    System.out.println(position);
                    Game.getJoueur();
                }
            }, timeWarning);

            timer.schedule(new TimerTask() {
                public void run() {
                    delete();
                    etat = false;
                    System.out.println(etat);
                }
            }, timeWarning+timeGlobal);
        }


        return;
    }


    /*fait disparaitre le laser
     */
    public void delete() {
        System.out.println("plus de laser");
        Game.enleverLaser(this);

    }

    /*compare les coordonn�s du joueur et du laser pour voir si il y a collision
     */

    public boolean checkCollision(double PlayerX, double PlayerY, int playerWidth){
        float coordx = this.getCoordX();
        float coordy = this.getCoordY();
        if(etat==true) {

            if(position==false) {
                System.out.println("vertical :" + (coordy+width));
                if(PlayerY >= coordy && PlayerY <= coordy+width ){
                    System.out.println("ok");

                    System.out.println("Touch�");
                    return true;
                } else if(PlayerX >= coordx && PlayerX <= coordx+width ){
                    System.out.println("Touch�");
                    return true;
                } else {
                    return false;
                }


            }}
        return false;
    }

    public int getId(){
        return id;
    }

    public Rectangle getSprite(){
        return sprite;
    }
}
