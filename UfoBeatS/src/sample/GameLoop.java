package sample;

import javafx.scene.shape.Rectangle;

import java.awt.*;

public class GameLoop implements Runnable{

    private boolean running;
    private final double updateRate = 1.0d/60.0d;

    private long nextStartTime;
    private int fps, ups;

    private int compteur;
    private  int compteur2;


    public void run() {
        running = true;
        double accumulator =0;
        long currentTime,lastUpdate = System.currentTimeMillis();
        nextStartTime = System.currentTimeMillis()+1000;

        while(running) {
            currentTime = System.currentTimeMillis();
            double lastRenderTimeInSeconds = (currentTime - lastUpdate)/1000d;
            accumulator += lastRenderTimeInSeconds;
            lastUpdate = currentTime;
            while(accumulator > updateRate) {
                update();
                accumulator -= updateRate;
            }
            printStats();
        }
    }

    private void printStats() {
        if(System.currentTimeMillis()> nextStartTime) {
            System.out.println(String.format("----------------------------------------------------------------------------"));
            System.out.println(String.format("FPS: %d, UPS %d", fps, ups));
            fps = 0;
            ups = 0;
            nextStartTime = System.currentTimeMillis()+1000;
            Laser laser = new Laser(100, 100, 0, 0, 0, 0, true);



            //Projectile projectile = new Projectile(500, 500, 0, 0, 0, 0);
            //Pattern.circle(20, projectile);



        }
    }


    private void render() {
        fps++;

    }

    private void update() {
        ups++;

        if(ups%2 ==0){
            Projectile projectile = new Projectile(500, 500, 0, 0, 2, 0);
            //Pattern.spiral(50, projectile, 0, true, compteur);

            /*
            Pattern.spiral(50, projectile, 0, true, compteur);
            Pattern.spiral(50, projectile, 0, false, compteur);

             */



            Pattern.spiral(50, projectile, 0, true, compteur);
            Pattern.spiral(50, projectile, 0, false, compteur);
            Pattern.spiral(50, projectile, Math.PI, true, compteur);
            Pattern.spiral(50, projectile, Math.PI, false, compteur);







            compteur++;
        }
        /*
        if(ups%15 == 0){
            Laser laser = new Laser(50, 50, 0, 25, 1000  , 2000, true);

            Pattern.damier(5, laser, compteur2);
            compteur2++;
        }

         */

        Game.updateSprite(0, 0, 0);
    }

    private void niveau(int ups, int compteur){

    }
}