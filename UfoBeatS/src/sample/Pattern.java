package sample;

import static java.lang.Math.round;

public class Pattern {

    public static void circle(int nbBullet, Projectile projectile) {	// tire n projectiles en rond
        /* En param�tre: le nombre de projectile, le projectile de base (seul le vecteur est modifier) */
        double angle = 2*Math.PI / nbBullet;
        double theta;

        Projectile pro;

        for(int i=0; i<nbBullet; i++) {
            theta = angle * i+1;


            pro = projectile;
            pro.setVectorX((float) Math.sin(theta)*1);
            pro.setVectorY((float) Math.cos(theta)*1);
            pro.setVelocite(3);
            Game.addProjectile(pro);// On ajoute � la liste de projectile
        }
    }

    public static void spiral(int nbBullet, Projectile projectile, double startPoint, boolean direction, int compteur) { // n projectile, lanc� tout les x secondes
        // startPoint en radiant  < 2 pi radiant
        // direction true = droite, false = gauche
        angle = 2*Math.PI / nbBullet
        double angle;
        if(direction) {
            angle = -angle;
      }

        double theta;
        Projectile pro;

        theta = angle * compteur+1%nbBullet + startPoint;

        pro = projectile;
        pro.setVectorX((float) Math.sin(theta)*1);
        pro.setVectorY((float) Math.cos(theta)*1);
        pro.setVelocite(5);
        Game.addProjectile(pro);
    }

    public static void damier(int nbLaser, Laser laser, int compteur){
        int x = WindowArea.getLongueurEcran() / nbLaser;
        int y = WindowArea.getLargeurEcran() / nbLaser;

        Laser las;

        las = laser;
        las.setCoordX(0);
        las.setCoordY(y * compteur);

        las.setPosition(false);
        Game.addlaser(las);

        las.setCoordX(x * compteur);
        las.setCoordY(0);

        las.setPosition(true);
        Game.addlaser(las);
    }

        /*
    private void targerPlayer(Projectile projectile) { // tire en direction du joueur
        Projectile pro = projectile;
        pro.setVectorX(pro.X - Player.x, pro.Y - Player.Y);
        pro.setVectorY(pro.X - Player.x, pro.Y - Player.Y);

        Game.ajoutProjectile(pro);
    }
    */





}
