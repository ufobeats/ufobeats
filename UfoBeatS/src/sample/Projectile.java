package sample;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.HashMap;
import java.util.ArrayList;

public class Projectile extends Movable implements Obstacle {
    private int id;
    private int acceleration;
    private static int nbProjectile= 0;
    private int delay;

    private Circle sprite;       // définition du joueur



    public Projectile(int x, int y, int vectorX, int vectorY, double velocite, int acceleration){
        super(x, y, vectorX, vectorY, velocite);

        this.acceleration = acceleration;
        id = nbProjectile;
        sprite = new Circle(x, y, 6, Color.RED);

        nbProjectile++;

    }

    public void moveSprite(){


        float x = getCoordX();
        float y = getCoordY();

        float newX = (float) (x+this.getVectorX()*this.getVelocite()+acceleration);
        float newY = (float) (y+this.getVectorY()*this.getVelocite()+acceleration);

        this.setCoordX(newX);
        this.setCoordY(newY);
        Game.setPosition(this);
        acceleration += acceleration;
        this.inScreen();

    }
/*
    public void checkCollision(int playerX , int playerY, int playerWidth) {
        if (playerX < this.getCoordX() + this.getWidthx() &&
                playerX + playerWidth > this.getCoordX() &&
                playerY < this.getCoordY() + this.getWidthx() &&
                playerWidth + playerY >this.getCoordY()){
            this.remove();
        }
    }
*/

    public void inScreen(){
        float x = this.getCoordX();
        float y = this.getCoordY();
        if (x < -5 || x > WindowArea.getLongueurEcran()  || y < -5 || y > WindowArea.getLargeurEcran()){
            this.remove();
        }
    }

    public void remove(){
        Game.setListProjectileUsed(id);
    }

    public int getId(){
        return this.id;
    }

    public Circle getSprite(){
        return sprite;
    }

    public int getAcceleration(){
        return acceleration;
    }

    public void setDelay(int time){
        System.out.println("je set à" + time);
        delay = time;
        System.out.println("donc je suis à " + delay);
    }

    public int getDelay(){
        return delay;
    }




}
