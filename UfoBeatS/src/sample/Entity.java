package sample;

import java.util.ArrayList;

public abstract class Entity {
    private float coordX ;
    private float coordY ;

    private int widthx;

    private float vectorX;
    private float vectorY;


    public Entity(int coordX, int coordY,float x, float y) {
        this.coordX = coordX;
        this.coordY = coordY;

        this.vectorX = x;
        this.vectorY = y;
        //this.vecteur = adjustVector(x, y);
    }


    public float getCoordX(){
        return this.coordX;
    }

    public void setCoordX(float x) {
        this.coordX = x;
    }

    public float getCoordY() {
        return coordY;
    }

    public void setCoordY(float coordY) {
        this.coordY = coordY;
    }



    public float getVectorX(){
        return vectorX;
    }

    public float getVectorY(){
        return vectorY;
    }

    public void setVectorX(float vector){
        vectorX = vector;
    }

    public void setVectorY(float vector){
        vectorY = vector;
    }


    public int getWidthx() {
        return widthx;
    }



    public ArrayList<Float> adjustVector(float x, float y){
        ArrayList<Float> vecteur= new ArrayList<Float>();
        float coeff;
        if(x > y) {
            coeff = 1 / x;
        }

        else {
            coeff = 1 / y;
        }

        x *= coeff;
        y = coeff;

        vecteur.add(x);
        vecteur.add(y);

        return vecteur;
    }


}
